# Contributing to dependabot-gitlab
We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features
- Becoming a maintainer

## We Develop with GitLab
We use GitLab to host code, to track issues and feature requests, as well as accept merge requests.

## All Code Changes Happen Through Merge Requests
Merge requests are the best way to propose changes to the codebase. We actively welcome your merge requests:

1. Fork the repo and create your branch from `main`.
2. If you've added code that should be tested, add tests.
3. If you've changed APIs, update the documentation.
4. Ensure pipeline passes.
5. Submit that merge request!

## Any contributions you make will be under the MIT Software License
In short, when you submit code changes, your submissions are understood to be under the same [MIT License](http://choosealicense.com/licenses/mit/) that covers the project. Feel free to contact the maintainers if that's a concern.

## Report bugs using GitLabs's [issues](https://docs.gitlab.com/ee/user/project/issues)
We use GitLab issues to track public bugs. Report a bug by [opening a new issue](https://gitlab.com/dependabot-gitlab/dependabot/-/issues/new); it's that easy!

## Use provided templates when submitting issues
Issues have predefined templates, please use those when submitting a new issues. The more information you provide, the more chance for a certain feature to be implemented or a certain bug to be fixed.

## License
By contributing, you agree that your contributions will be licensed under its MIT License.

## References
This document was adapted from the basic open-source contribution [template](https://gist.github.com/briandk/3d2e8b3ec8daf5a27a62)
