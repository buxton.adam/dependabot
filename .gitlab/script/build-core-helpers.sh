#!/bin/bash

set -e

source "$(dirname "$0")/utils.sh"

core_revision="$(dependabot_revision)"
core_zip="tmp/dependabot.zip"
helpers=("$@")

log_with_header "Setting up native build helpers v${core_revision}"
log "** Downloading native helpers **"
curl -s --fail --location --output $core_zip \
  "https://github.com/dependabot/dependabot-core/archive/${core_revision}.zip"
log_success "success!\n"

log "** Extracting native helpers **"
for helper in "${helpers[@]}"; do
  log_info "extracting $helper"
  unzip -q -o $core_zip "dependabot-core-${core_revision}/${helper}/helpers/*" -d tmp/
done
log_success "success!\n"

log "** Building native helpers **"
export DEPENDABOT_NATIVE_HELPERS_PATH="helpers"
for helper in "${helpers[@]}"; do
  log_info "building $helper"

  if [ "$helper" == "bundler" ]; then
    path=tmp/dependabot-core-${core_revision}/${helper}/helpers/v2/build
  else
    path=tmp/dependabot-core-${core_revision}/${helper}/helpers/build
  fi

  bash $path
  log_info "done!"
  echo
done
log_success "success!"

rm -rf $core_tgz tmp/dependabot-core-${core_revision}
