ARG BASE_IMAGE=ghcr.io/andrcuns/dependabot-updater-bundler:latest

FROM ${BASE_IMAGE} as core

FROM core as production

WORKDIR /home/dependabot/app

ENV BUNDLE_PATH=vendor/bundle \
    BUNDLE_WITHOUT="development:test" \
    RAILS_ENV=production

# Copy gemfile first so cache can be reused
COPY --chown=dependabot:dependabot Gemfile Gemfile.lock ./
RUN set -eux; \
    bundle install \
    && gem clean \
    && bundle clean \
    && rm -rf vendor/bundle/ruby/3.1.0/cache

COPY --chown=dependabot:dependabot ./ ./

# Smoke test image
RUN SETTINGS__GITLAB_ACCESS_TOKEN=token bundle exec rake about

ARG COMMIT_SHA
ARG PROJECT_URL
ARG VERSION
ARG BASE_IMAGE

ENV APP_VERSION=$VERSION

LABEL org.opencontainers.image.authors="andrejs.cunskis@gmail.com" \
    org.opencontainers.image.source=$PROJECT_URL \
    org.opencontainers.image.revision=$COMMIT_SHA \
    org.opencontainers.image.version=$VERSION \
    org.opencontainers.image.base.name=$BASE_IMAGE

ENTRYPOINT [ "bundle", "exec" ]
