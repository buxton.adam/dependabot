# frozen_string_literal: true

module Gitlab
  module MergeRequest
    # :reek:LongParameterList
    # rubocop:disable Metrics/ParameterLists

    class Creator < ApplicationService
      MR_OPTIONS = %i[
        custom_labels
        branch_name_separator
        branch_name_prefix
        branch_name_max_length
      ].freeze

      # @param [Project] project
      # @param [Dependabot::Files::Fetchers::Base] fetcher
      # @param [Hash] config_entry
      # @param [Dependabot::UpdatedDependency] updated_dependency
      # @param [Number] target_project_id
      def initialize(project:, fetcher:, config_entry:, updated_dependency:, credentials:, target_project_id:)
        @project = project
        @fetcher = fetcher
        @config_entry = config_entry
        @updated_dependency = updated_dependency
        @credentials = credentials
        @target_project_id = target_project_id
      end

      delegate :commit_message, :branch_name, to: :gitlab_creator

      # Create merge request
      #
      # @return [Gitlab::ObjectifiedHash]
      def call
        gitlab_creator.create
      end

      private

      delegate :vulnerable?, :production?, to: :updated_dependency

      attr_reader :project,
                  :fetcher,
                  :updated_dependency,
                  :config_entry,
                  :credentials,
                  :target_project_id

      # Gitlab merge request creator
      #
      # @return [Dependabot::PullRequestCreator::Gitlab]
      def gitlab_creator
        @gitlab_creator ||= Dependabot::PullRequestCreator.new(
          source: fetcher.source,
          base_commit: fetcher.commit,
          dependencies: updated_dependency.updated_dependencies,
          files: updated_dependency.updated_files,
          credentials: credentials,
          github_redirection_service: "github.com",
          pr_message_footer: message_footer,
          automerge_candidate: updated_dependency.auto_mergeable?,
          vulnerabilities_fixed: updated_dependency.fixed_vulnerabilities,
          provider_metadata: { target_project_id: target_project_id },
          **mr_options
        ).send(:gitlab_creator)
      end

      # Get assignee ids
      #
      # @return [Array<Number>]
      def assignees
        UserFinder.call(config_entry[:assignees])
      end

      # Get reviewer ids
      #
      # @return [Array<Number>]
      def reviewers
        UserFinder.call(config_entry[:reviewers])
      end

      # Get approver ids
      #
      # @return [Array<Number>]
      def approvers
        UserFinder.call(config_entry[:approvers])
      end

      # Milestone id
      #
      # @return [Integer]
      def milestone_id
        MilestoneFinder.call(project, config_entry[:milestone])
      end

      # Merge request specific options from config
      #
      # @return [Hash]
      def mr_options
        {
          label_language: true,
          assignees: assignees,
          reviewers: { approvers: approvers, reviewers: reviewers }.compact,
          milestone: milestone_id,
          commit_message_options: commit_message_options,
          **config_entry.slice(*MR_OPTIONS)
        }
      end

      # Commit message options
      #
      # @return [Hash]
      def commit_message_options
        opts = config_entry[:commit_message_options].dup
        return {} unless opts

        trailers_security = opts.delete(:trailers_security)
        trailers_dev = opts.delete(:trailers_development)
        return opts.merge({ trailers: trailers_security }) if vulnerable? && trailers_security
        return opts.merge({ trailers: trailers_dev }) if !production? && trailers_dev

        opts
      end

      # Squash merge request on auto-merge
      #
      # @return [Boolean]
      def squash?
        return false unless updated_dependency.auto_mergeable?

        config_entry.dig(:auto_merge, :squash).nil? ? false : true
      end

      # MR message footer with available commands
      #
      # @return [String]
      def message_footer; end
    end
  end
  # rubocop:enable Metrics/ParameterLists
end

Gitlab::MergeRequest::Creator.prepend(Gitlab::MergeRequest::ServiceMode::Creator) if AppConfig.service_mode?
