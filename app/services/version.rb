# frozen_string_literal: true

class Version
  VERSION = "1.0.0-alpha.2"
end
