# frozen_string_literal: true

require "securerandom"

module Updater
  module Kubernetes
    class Runner < RunnerBase
      def initialize(...)
        @container_startup_timeout = UpdaterConfig.updater_container_startup_timeout

        super
      end

      def call
        super

        run_updater
        wait_for_completion
      ensure
        delete_pod
      end

      private

      attr_reader :container_startup_timeout

      # Kubernetes client
      #
      # @return [Kubeclient::Client]
      def client
        @client ||= Client.new
      end

      # Updater pod name
      #
      # @return [String]
      def pod_name
        @pod_name ||= resource_hash.dig(:metadata, :name)
      end

      # Deployment namespace
      #
      # @return [String]
      def namespace
        @namespace ||= resource_hash.dig(:metadata, :namespace)
      end

      # Pod resource definition hash
      #
      # @return [Hash]
      def resource_hash
        @resource_hash ||= YAML.safe_load(pod_yml, symbolize_names: true)
      end

      # Pod template yml
      #
      # @return [String]
      def pod_yml
        @pod_yml ||= format(
          File.read(UpdaterConfig.updater_template_path),
          project_name: project_name,
          package_ecosystem: package_ecosystem,
          directory: directory,
          name_sha: unique_name_postfix
        )
      end

      # Run updater
      #
      # @return [void]
      def run_updater
        log(:info, "Creating updater pod '#{pod_name}'")
        pod = client.create_pod(Kubeclient::Resource.new(resource_hash))

        log(:info, "Waiting for updater container to start")
        wait_for_startup(pod)
        stream_log
      end

      # Get updater pod
      #
      # @return [Kubeclient::Resource]
      def updater_pod
        client.get_pod(pod_name, namespace)
      end

      # Wait for updater container to start
      #
      # @param [Kubeclient::Resource] pod
      # @return [void]
      def wait_for_startup(pod)
        tries = 1

        while pod.status.phase == "Pending"
          if tries > container_startup_timeout
            raise(UpdaterFailure, "Updater container failed to start: #{pod.status.containerStatuses}")
          end

          log(:debug, "  updater container is still pending, waiting...")
          tries += 1
          pod = updater_pod
          sleep(1)
        end
      end

      # Stream updater container log
      #
      # @return [void]
      def stream_log
        client.watch_pod_log(pod_name, namespace) { |line| puts line.strip } # rubocop:disable Rails/Output
      rescue Kubeclient::HttpError => e
        log_error(e, message_prefix: "Failed to attach updater container log, update output will not be streamed")
      end

      # Wait for pod finish status
      #
      # @return [void]
      def wait_for_completion
        phase = updater_pod.status.phase

        log(:debug, "Waiting for updater pod completion")
        while phase == "Running"
          log(:debug, "  pod is still running, waiting...")
          sleep(1)
          phase = updater_pod.status.phase
        end
        raise(UpdaterFailure, "Updater pod did not finish successfully, phase: '#{phase}'") unless phase == "Succeeded"
      end

      # Delete updater pod
      #
      # @return [void]
      def delete_pod
        log(:debug, "Deleting pod '#{pod_name}'")
        client.delete_pod(pod_name, namespace) if UpdaterConfig.delete_updater_container?
      rescue StandardError => e
        log_error(e, message_prefix: "Failed to delete updater pod '#{pod_name}'")
      end
    end
  end
end
