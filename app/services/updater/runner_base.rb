# frozen_string_literal: true

module Updater
  class UpdaterFailure < StandardError; end

  class RunnerBase < ApplicationService
    def initialize(project_name, package_ecosystem, directory)
      @project_name = project_name
      @package_ecosystem = package_ecosystem
      @directory = directory
    end

    def call
      log(:info, "Starting dependency updater")
    end

    private

    attr_reader :project_name, :package_ecosystem, :directory

    # Unique name postfix for updater
    #
    # @return [String]
    def unique_name_postfix
      @unique_name_postfix ||= "#{SecureRandom.alphanumeric(9)}-#{SecureRandom.alphanumeric(5)}".downcase
    end
  end
end
