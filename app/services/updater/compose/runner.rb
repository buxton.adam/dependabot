# frozen_string_literal: true

require "yaml"
require "open3"

module Updater
  module Compose
    class Runner < RunnerBase
      def call
        super

        log(:debug, "Starting updater container using image '#{updater_image}'")
        Open3.popen3(*compose_run_cmd) do |stdin, out, err, wait_thr|
          stdin.write(YAML.dump(updater_conf))
          stdin.close

          out.each { |line| puts line.strip } # rubocop:disable Rails/Output
          next if wait_thr.value.success?

          log(:error, "Updater process finished with non 0 result!")
          raise(UpdaterFailure, err.read)
        end
      end

      private

      attr_reader :compose_project_name

      # Compose run command
      #
      # @return [String]
      def compose_run_cmd
        cmd_base = [
          "docker", "compose", "-f", "-", "run", "--no-TTY", "--quiet-pull",
          "--name", "updater-#{package_ecosystem}-#{unique_name_postfix}"
        ]
        cmd_base << "--rm" if UpdaterConfig.delete_updater_container?

        [
          *cmd_base, "updater",
          "bundle", "exec", "rake",
          "dependabot:update[#{project_name},#{package_ecosystem},#{directory}]"
        ]
      end

      # Raw compose yml
      #
      # @return [Hash]
      def compose_yml
        @compose_yml ||= YAML.load_file(UpdaterConfig.updater_template_path, aliases: true)
      end

      # Updater container configuration
      #
      # @return [Hash]
      def updater_conf
        @updater_conf ||= {
          "version" => compose_yml["version"],
          "services" => {
            "updater" => {
              "image" => updater_image,
              **compose_yml["x-updater"]
            }
          }
        }
      end

      # Updater docker image
      #
      # @return [String]
      def updater_image
        @updater_image ||= format(UpdaterConfig.updater_image_pattern, package_ecosystem: package_ecosystem)
      end
    end
  end
end
