# frozen_string_literal: true

module Dependabot
  module ServiceMode
    module UpdateService
      # Project
      #
      # @return [Project]
      def project
        @project ||= Project.find_by(name: project_name).tap do |existing_project|
          existing_project.configuration = fetch_config unless AppConfig.integrated?
        end
      end

      # Vulnerability alerts enabled
      #
      # @return [Boolean]
      def vulnerability_alerts?
        config_entry.dig(:vulnerability_alerts, :enabled)
      end

      # Handle vulnerability issues
      #
      # @param [Dependabot::Dependencies::UpdatedDependency] updated_dep
      # @return [void]
      def handle_vulnerability_issues(updated_dep)
        return if DependabotConfig.dry_run?

        if updated_dep.vulnerable? && !vulnerability_alerts?
          log(:warn, "  dependency has vulnerability but dependabot was not able to update the version!")
          return
        end

        create_vulnerability_issues(updated_dep) if updated_dep.vulnerable?
        close_obsolete_vulnerability_issues(updated_dep)
      end

      # Create security vulnerability alert issues
      #
      # @param [Dependabot::Dependencies::UpdatedDependency] dependency
      # @return [void]
      def create_vulnerability_issues(dependency)
        log(:info, "  creating vulnerability issues for dependency")
        dependency.actual_vulnerabilities.each do |vulnerability|
          Gitlab::Vulnerabilities::IssueCreator.call(
            project: project,
            vulnerability: vulnerability,
            dependency_file: dependency.dependency_files.reject(&:support_file).first,
            assignees: config_entry.dig(:vulnerability_alerts, :assignees),
            confidential: config_entry.dig(:vulnerability_alerts, :confidential)
          )
        rescue Gitlab::Error::ResponseError => e
          log_error(e, message_prefix: "   failed to create issues for vulnerabilities: #{vulnerability.identifiers}")
        end
      end

      # Close obsolete vulnerability alerts
      #
      # @param [Dependabot::Dependencies::UpdatedDependency] dependency
      # @return [void]
      def close_obsolete_vulnerability_issues(dependency)
        vulnerability_issues = project.open_vulnerability_issues(
          package_ecosystem: package_ecosystem,
          directory: directory,
          package: dependency.name
        )
        obsolete_issues = vulnerability_issues.reject { |issue| issue.vulnerability.vulnerable?(dependency.version) }
        return if obsolete_issues.empty?

        log(:info, "  closing obsolete vulnerability issues")
        obsolete_issues.each do |issue|
          log(:debug, "   closing obsolete issue !#{issue.iid} because dependency version is not vulnerable")
          Gitlab::Vulnerabilities::IssueCloser.call(issue)
        rescue Gitlab::Error::ResponseError => e
          log_error(e, message_prefix: "   failed to close vulnerability issue")
        end
      end

      # Close obsolete merge requests
      #
      # @param [String] dependency_name
      # @return [void]
      def close_obsolete_mrs(dependency_name)
        return if DependabotConfig.dry_run?

        obsolete_mrs = project.open_dependency_merge_requests(dependency_name, directory)
        return if obsolete_mrs.empty?

        log(:info, "  closing obsolete merge requests")
        obsolete_mrs.each do |mr|
          log(:debug, "   closing obsolete merge request !#{mr.iid} because dependency version is up to date")
          mr.close
          Gitlab::BranchRemover.call(project_name, mr.branch)
        rescue Gitlab::Error::ResponseError => e
          log_error(e, message_prefix: "   failed to close obsolete merge request")
        end
      end
    end
  end
end
