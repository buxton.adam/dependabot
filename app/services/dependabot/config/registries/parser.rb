# frozen_string_literal: true

module Dependabot
  module Config
    module Registries
      # Type mapping from yml values in config file to values accepted by dependabot core
      #
      # @return [Hash]
      TYPE_MAPPING = {
        "maven-repository" => { type: "maven_repository", url: "url" },
        "docker-registry" => { type: "docker_registry", url: "registry" },
        "npm-registry" => { type: "npm_registry", url: "registry" },
        "composer-repository" => { type: "composer_repository", url: "registry" },
        "git" => { type: "git_source", url: "host" },
        "nuget-feed" => { type: "nuget_feed", url: "url" },
        "python-index" => { type: "python_index", url: "index-url" },
        "rubygems-server" => { type: "rubygems_server", url: "host" },
        "terraform-registry" => { type: "terraform_registry", url: "host" },
        "hex-organization" => { type: "hex_organization" },
        "hex-repository" => { type: "hex_repository" }
      }.freeze

      # Registries configuration parser
      #
      class Parser < ApplicationService
        def initialize(registries)
          @registries = registries
        end

        # Fetch configured registries
        #
        # @return [Hash]
        def call
          return {} unless registries

          registries
            .stringify_keys
            .transform_values { |registry| transform_registry_values(registry) }
            .compact
        end

        private

        # @return [Array]
        attr_reader :registries

        # Update registry hash
        #
        # dependabot-core uses specific credentials hash depending on registry types with keys as strings
        #
        # @param [Hash] registry
        # @return [Hash]
        def transform_registry_values(registry)
          type = registry[:type]
          mapped_type = TYPE_MAPPING[type]
          return warn_unsupported_registry(type) unless mapped_type
          return warn_incorrect_registry(type) unless registry_config_valid?(registry)

          transformers.fetch(type, transformers["default"]).call(registry, mapped_type).compact
        end

        # Log warning for partially configured credentials
        #
        # @param [String] type
        # @return [nil]
        def warn_incorrect_registry(type)
          log(:error, "Got partially or incorrectly configured registry '#{type}'")
          nil
        end

        # Log warning for unsupported registry type
        #
        # @param [String] type
        # @return [nil]
        def warn_unsupported_registry(type)
          log(:error, "Registry type '#{type}' is not supported!")
          nil
        end

        # Check if registry credentials block is valid
        #
        # @param [Hash] registry
        # @return [Boolean]
        def registry_config_valid?(registry)
          [
            PrivateRegistries::NoAuthContract.new.call(registry),
            PrivateRegistries::TokenContract.new.call(registry),
            PrivateRegistries::PasswordContract.new.call(registry),
            PrivateRegistries::KeyContract.new.call(registry),
            PrivateRegistries::HexPrivateRepositoryContract.new.call(registry)
          ].any?(&:success?)
        end

        # Strip protocol from registries of specific type
        #
        # Private npm and docker registries will not work if protocol is defined
        #
        # @param [String] type
        # @param [String] url
        # @return [String]
        def strip_protocol(type, url)
          return url unless %w[npm-registry docker-registry terraform-registry].include?(type)

          url.gsub(%r{https?://}, "")
        end

        # :reek:DuplicateMethodCall

        # Registry value transformers
        #
        # @return [Hash]
        def transformers
          @transformers ||= {
            "default" => lambda do |registry, mapped_type|
              {
                "type" => mapped_type[:type],
                mapped_type[:url] => strip_protocol(registry[:type], registry[:url]),
                **registry.except(:type, :url).transform_keys(&:to_s)
              }
            end,
            "hex-organizations" => lambda do |registry, mapped_type|
              {
                "type" => mapped_type[:type],
                "organization" => registry[:organization],
                "token" => registry[:key]
              }
            end,
            "hex-repository" => lambda do |registry, mapped_type|
              {
                "type" => mapped_type[:type],
                **registry.except(:type).transform_keys { |key| key.to_s.tr("-", "_") }
              }
            end,
            "python-index" => lambda do |registry, mapped_type|
              token = registry[:token] || "#{registry[:username]}:#{registry[:password]}"

              {
                "type" => mapped_type[:type],
                "token" => token,
                "replaces-base" => registry[:"replaces-base"] || false,
                mapped_type[:url] => registry[:url]
              }
            end
          }
        end
      end
    end
  end
end
