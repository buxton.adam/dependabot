# frozen_string_literal: true

describe Dependabot::Config::Registries::Parser, epic: :services, feature: :dependabot do
  subject(:parsed_registries) { described_class.call(registries) }

  context "with correctly configured registries" do
    let(:registries) do
      {
        dockerhub: {
          type: "docker-registry",
          url: "https://registry.hub.docker.com",
          username: "octocat",
          password: "password"
        },
        npm: {
          type: "npm-registry",
          url: "https://npm.pkg.github.com",
          token: "${{NPM_TEST_TOKEN}}"
        }
      }
    end

    it "returns parsed registries" do
      expect(parsed_registries).to eq(
        {
          "dockerhub" => {
            "type" => "docker_registry",
            "registry" => "registry.hub.docker.com",
            "username" => "octocat",
            "password" => "password"
          },
          "npm" => {
            "type" => "npm_registry",
            "registry" => "npm.pkg.github.com",
            "token" => "${{NPM_TEST_TOKEN}}"
          }
        }
      )
    end
  end

  context "with partially configured credentials" do
    let(:registries) do
      {
        maven: {
          type: "maven-repository",
          url: "https://maven-repo.com"
        },
        npm: {
          type: "npm-registry",
          url: "https://npm.pkg.github.com",
          username: "test"
        }
      }
    end

    it "filters out incorrect partial credentials" do
      expect(parsed_registries).to eq(
        {
          "maven" => {
            "type" => "maven_repository",
            "url" => "https://maven-repo.com"
          }
        }
      )
    end
  end

  context "with hex-organizations registry" do
    let(:registries) do
      {
        hex: {
          type: "hex-organization",
          organization: "org",
          key: "key"
        }
      }
    end

    it "correctly transforms registry definition" do
      expect(parsed_registries).to eq({
        "hex" => {
          "type" => "hex_organization",
          "organization" => "org",
          "key" => "key"
        }
      })
    end
  end

  context "with hex-repository registry" do
    let(:registries) do
      {
        hex: {
          type: "hex-repository",
          organization: "private-repo",
          url: "https://private-repo.example.com",
          "auth-key": "key",
          "public-key-fingerprint": "fingerprint"
        }
      }
    end

    it "correctly transforms registry definition" do
      expect(parsed_registries).to eq({
        "hex" => {
          "type" => "hex_repository",
          "organization" => "private-repo",
          "url" => "https://private-repo.example.com",
          "auth_key" => "key",
          "public_key_fingerprint" => "fingerprint"
        }
      })
    end
  end

  context "with python-index registry" do
    let(:registries) do
      {
        python: {
          type: "python-index",
          url: "https://example.com/_packaging/my-feed/pypi/example",
          username: "octocat",
          password: "password",
          "replaces-base": true
        }
      }
    end

    it "correctly transforms registry definition" do
      expect(parsed_registries).to eq({
        "python" => {
          "type" => "python_index",
          "index-url" => "https://example.com/_packaging/my-feed/pypi/example",
          "token" => "octocat:password",
          "replaces-base" => true
        }
      })
    end
  end
end
