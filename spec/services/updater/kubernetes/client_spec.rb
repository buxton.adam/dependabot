# frozen_string_literal: true

describe Updater::Kubernetes::Client do
  subject(:client) { described_class }

  let(:kubeclient) { double("Kubeclient::Client", create_pod: nil) } # rubocop:disable RSpec/VerifiedDoubles

  before do
    allow(Kubeclient::Client).to receive(:new).and_return(kubeclient)
    allow(File).to receive(:exist?).with("/var/run/secrets/kubernetes.io/serviceaccount/ca.crt").and_return(true)
  end

  it "initializes kubeclient" do
    client.new

    expect(Kubeclient::Client).to have_received(:new).with(
      "https://kubernetes.default.svc",
      "v1",
      auth_options: { bearer_token_file: "/var/run/secrets/kubernetes.io/serviceaccount/token" },
      ssl_options: { ca_file: "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt" }
    )
  end

  it "passes methods to kubeclient" do
    client.new.create_pod

    expect(kubeclient).to have_received(:create_pod)
  end
end
